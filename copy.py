import csv
import os
import logging


def Merge_csv(path,filename, destination):
    '''This function is used to merge the CSV files having same columns in same order in every CSV file
        The inputs this function takes are:
        1. path:  Give the path of folder in which you have csv files
        2. filename: Give the filename (without extension) you want your merged file to save as
        3. destination: Give the destination folder name where you want to save your merged file
        '''
    try:
        a=os.listdir(path)
        os.chdir(path)
        m = 1
        for i in a:
            if i[-3:]=="csv":
                s=open(i, mode='r')
                csvFile = csv.reader(s)
                with open(f'{destination}/{filename}.csv', 'a+', newline="") as csvfile:
                    csvwriter = csv.writer(csvfile)

                    for p in csvFile:
                        if p==['user_type', 'first_name', 'last_name', 'extension', 'client_id', 'client_email', 'client_password', 'ip', 'phone_type', 'did', 'vm_password', 'tenant_id', 'Instance', 'registration', 'ldvs', 'zone']:
                            break
                        else:
                            logging.info(f"{i} has column names mismatch with the standard columns")
                            f=1
                            break
                    try:
                        if f==1:
                            continue
                    except Exception as e:
                        pass
                    if m==1:
                        csvwriter.writerows(csvFile)
                        m+=1
                    else:
                        k = 1
                        for j in csvFile:
                            if k==1:
                                pass
                                k+=1
                            else:
                                csvwriter.writerow(j)
            else:
                print(f"{i} is not a csv format file..")
                logging.info(f"{i} file is not a csv format file..")

    except Exception as e:
        print(e)

        logging.error(e)



if __name__=="__main__":
    logging.basicConfig(level=logging.INFO, filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
    path = input("Enter the path of folder having the csv files: ")
    filename= input("Enter the name of merged file(without extension) with which you want to Save As: ")
    destination= input("Enter the destination folder where you want to save your merged file: ")
    Merge_csv(path, filename, destination)