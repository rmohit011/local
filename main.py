import pandas as pd
import os

def MergeCSV():
    path=input("Please provide the path of folder having multiple csv files...")
    lis=os.listdir(path)
    os.chdir(path)
    df = pd.concat(map(pd.read_csv, lis), ignore_index=True)
    df.to_csv("Merged.csv", index=False)
    print("changes done")


if __name__=="__main__":
    MergeCSV()