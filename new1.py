import csv
import os
import logging

def Merge_csv(path,filename, destination):
    '''This function is used to merge the CSV files having standard column names in same order in every CSV file and
        create a app.log file for logging issues in destination folder location given as input by user.
        The inputs this function takes are:
        1. path:  Give the path of folder in which you have csv files
        2. filename: Give the filename (without extension) you want your merged file to save as
        3. destination: Give the destination folder name where you want to save your merged file

        The output of this function are:
        1. Merged csv file saved at destination with the filename provided by user
        2. app.log file saved at same destination where merged file is saved.
        '''
    try:
        files=os.listdir(path)
        os.chdir(path)
        csv_files=[]
        not_csv=[]
        for file in files:
            if file[-3:]=="csv":
                csv_files.append(file)
            else:
                logging.info(f"{file} is not a csv format file")
                print(f"{file} is not a csv format file")
                not_csv.append(file)

        with open(f'{destination}/{filename}.csv', 'a+', newline="") as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['user_type', 'first_name', 'last_name', 'extension', 'client_id', 'client_email',
                                'client_password', 'ip', 'phone_type', 'did', 'vm_password', 'tenant_id', 'Instance',
                                'registration', 'ldvs', 'zone'])

        for i in range(len(csv_files)):
                s=open(csv_files[i], mode='r')
                csvFile = csv.reader(s)
                with open(f'{destination}/{filename}.csv', 'a+', newline="") as csvfile:
                    csvwriter = csv.writer(csvfile)
                    lis = list(csvFile)
                    if lis[0] == ['user_type', 'first_name', 'last_name', 'extension', 'client_id', 'client_email',
                                  'client_password', 'ip', 'phone_type', 'did', 'vm_password', 'tenant_id', 'Instance',
                                  'registration', 'ldvs', 'zone']:
                        csvwriter.writerows(lis[1:])

                    else:
                        print(f"{csv_files[i]} columns name mismatch with the standard columns name")
                        logging.info(f"{csv_files[i]} columns name mismatch with the standard columns name")
                        continue
    except Exception as e:
        print(e)
        logging.error(e)

if __name__=="__main__":
    path = input("Enter the path of folder having the csv files: ")
    filename= input("Enter the name of merged file(without extension) with which you want to Save As: ")
    destination= input("Enter the destination folder where you want to save your merged file: ")
    logging.basicConfig(level=logging.INFO, filename=f'{destination}/app.log', filemode='w',format='%(name)s - %(levelname)s - %(message)s')
    Merge_csv(path, filename, destination)